const express = require("express");
const FluentClient = require("@fluent-org/logger").FluentClient;
const app = express();

const logger = new FluentClient("fluentd.test", {
  socket: {
    host: "164.92.184.178",
    port: 9880,
    timeout: 3000,
  },
});

app.get("/", function (request, response) {
  logger.emit("follow", { from: "userA", to: "userB" });
  response.send("Fluent test");
});
const port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log("Listening on " + port);
});
